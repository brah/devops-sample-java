/**
 * The Main class is the entry point of the application.
 * It contains the main method that prints "Hello, World!" to the console.
 *
 * @author John Doe
 * @version 1.0
 * @since 2021-10-01
 * @see <a href="https://example.com">Example Website</a>
 */
public class HelloWorld {

    /**
     * The main method prints "Hello, World!" to the console.
     *
     * @param args The command-line arguments.
     */
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
    
}