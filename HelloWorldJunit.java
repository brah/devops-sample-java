import static org.junit.Assert.assertEquals;  
import org.junit.Test;  
public class HelloWorldJunit {  
    @Test  
    public void testHello() {  
        String hello = "Helloworld";
        assertEquals("Test Helloworld", hello, "Helloworld");  
    }  
}
